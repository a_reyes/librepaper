
# NOTAS

---

## [SSD1680][1]

---

+ 176 Source x 296 Gate Red/Black/White
+ Active Matrix EPD Display Driver with
Controller.

+ 176 source outputs
+ 296 gate outputs
+ 1 VCOM
+ 1 VBD
+ On-chip display RAM (Mono B/W) 176x296 bits

## RAM

---

The on chip display RAm is holding the image data.
1 set of RAM is built for Mono B/W. The RAM size is 176x296 bits.
1 set of RAM is built for Mono Red.  The RAM size is 176x296 bits.

Table 6.5: RAM bit and LUT mapping for black/white display

|R RAM|B/W RAM|Image Color|  LUT |
|:----:|:----:|:---------:|:----:|
|0|0|Black|LUT 0|
|    0  |   1     |    White    | LUT 1|
|    1  |   0     |    Black    | LUT 2|
|    1  |   1     |    White    | LUT 3|


## Operation Flow

--- 

![Alt text](./docs/Figure%209-1%20Operation%20flow%20to%20drive%20display%20panel.png "Figure9-1")

---

# ToDo:

---

- [ ] Comunicacion i2c co controlador.
- [ ] Funcións de comunicación básica entre MCU-Controlador.
- [ ] Funcións para crear formas básicas.
- [ ] Funcións para crear secuencias de formas.
- [ ] Funcións para combinar formas básicas.
- [ ] Escribir documentación.

## Posibles exemplos:

---
a) Xogo da vida.  
b) Imaxe reloxo de area animado.  
c) Termómetro (o controlador ten unha función para ler a temperatura.)  

## Obxetivos:

---

- C++ 20
- Adecuado para MCU:
    + ESP32.
    + Renesas.
    + STM32 (Arduino).
- Baixo consumo de memoria.
- Velocidade.
- Fácil de usar e aprender.



[1]: https://www.solomon-systech.com/product/ssd1680-80a/
